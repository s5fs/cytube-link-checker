## Cytube Link Checker

Feed this script a CSV file of video URLs and it will tell you which ones didn't load correctly in cytube.

Note! This is an untested script, you may want to check the "bad" links manually.

Disclaimer: This has only been tested on OSX :)

### How to use

__Prequisites__

1. node.js version 14.x or greater
2. Export cookie firefox plugin - https://addons.mozilla.org/en-US/firefox/addon/%E3%82%AF%E3%83%83%E3%82%AD%E3%83%BCjson%E3%83%95%E3%82%A1%E3%82%A4%E3%83%AB%E5%87%BA%E5%8A%9B-for-puppeteer/
3. Working internet connection

__Install__

1. Install the dependencies
	`npm install`
2. Copy the example config file and add your cookies from cytu.be to the appropriate section.
	`cp config-example.json config.json`
3. Execute the script
	`npm start`

__Config Options__

The configuration file is `config.json`, it supports the following options:

`channel` - Set the name of the Cytube channel. If left blank, the channel name will be random.
`inputcsv` - CSV file full of URLs you wish to check
`outputcsv.good` - Output file location for the good URLs
`outputcsv.bad` - Output file location for the bad URLs
`timeout` - How long will we wait for a URL to be added to the playlist before we error out (in ms)
`cookies` - Export your browser cookies from cytu.be and stick 'em here
