const { SSL_OP_EPHEMERAL_RSA } = require('constants')
const { nanoid } = require('nanoid')
const config = require('./config.json')
const fs = require('fs')
const parse = require('csv-parse')
const puppeteer = require('puppeteer')

const channel = `https://cytu.be/r/${config.hasOwnProperty('channel') ? config.channel : nanoid()}`
const urls = {good: [], bad: []}

const processFile = async () => {
  records = []
  const parser = fs
  .createReadStream(config.inputcsv)
  .pipe(parse({
  }));
  for await (const record of parser) {
    records.push(record)
  }
  return records
}

(async () => {
  const records = await processFile()
  console.log('Launching puppeteer')
  const browser = await puppeteer.launch()

  // Open the page
  console.log('Opening browser')
  const page = await browser.newPage()

  // Set cookies
  console.log('Setting cookies')
  for (const cookie of config.cookies) {
    await page.setCookie(cookie)
  }

  // Navigate to Cytube test channel
  try {
    console.log('Going to ' + channel)
    await page.goto(channel, { waitUntil: 'networkidle0'})

    console.log('Waiting for the page to finish rendering')
    await page.waitForSelector('span.glyphicon-plus:nth-child(1)', {visible: true})
    await page.click('span.glyphicon-plus:nth-child(1)')
    await page.waitForSelector('#mediaurl', {visible: true})

    // Process URLs
    console.log(`Processing ${records[0].length} URLs`)
    let counter = 0
    for (url of records[0]) {
      counter++
      console.log(`Adding: ${url}`)
      await page.evaluate((url) => {
        document.querySelector('#mediaurl').value = url
      }, url)
      await page.click('#queue_end')

      // This is kinda shitty error handling...
      try {
        await page.waitForSelector('.alert-danger', {visible: true, timeout: config.timeout})
        await page.click('.alert-danger > button:nth-child(1)')
        await page.waitForTimeout(500)
        urls.bad.push(url)
      } catch (ex) {
        // console.log(`URL added successfully: ${url}`)
        urls.good.push(url)
      }

      // Status message
      if (counter === 10) {
        counter = 0
        console.log(`Status: ${urls.good.length + urls.bad.length} URLs processed (${urls.good.length} good, ${urls.bad.length} bad)`)
      }
    }

    console.log(`Finished! ${urls.good.length + urls.bad.length} URLs processed (${urls.good.length} good, ${urls.bad.length} bad)`)
    console.log('Writing output files to disk')
    // Write good files
    fs.writeFileSync(config['outputcsv.good'], urls.good.join(','))

    // Write bad files
    fs.writeFileSync(config['outputcsv.bad'], urls.bad.join(','))
  } catch (ex) {
    console.log('Error loading channel. Are the cookies in your cookies.json file current?')
    console.log(ex)
    await page.screenshot({path: 'error.png'})
  }

  // Close the page
  await browser.close()
})()
